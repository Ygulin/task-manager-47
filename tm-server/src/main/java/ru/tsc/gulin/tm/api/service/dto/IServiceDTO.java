package ru.tsc.gulin.tm.api.service.dto;

import ru.tsc.gulin.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {

}
