package ru.tsc.gulin.tm.api.repository.dto;

import ru.tsc.gulin.tm.dto.model.SessionDTO;

public interface ISessionRepositoryDTO extends IUserOwnedRepositoryDTO<SessionDTO> {
}
