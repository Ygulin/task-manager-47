package ru.tsc.gulin.tm.api.service.model;

import ru.tsc.gulin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
