package ru.tsc.gulin.tm.api.repository.model;

import ru.tsc.gulin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
