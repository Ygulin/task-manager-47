package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.gulin.tm.api.service.dto.ISessionServiceDTO;

public interface IServiceLocator {

    @NotNull
    ITaskServiceDTO getTaskService();

    @NotNull
    IProjectServiceDTO getProjectService();

    @NotNull
    IProjectTaskServiceDTO getProjectTaskService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserServiceDTO getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionServiceDTO getSessionService();

}
