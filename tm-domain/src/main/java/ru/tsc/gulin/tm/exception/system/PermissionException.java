package ru.tsc.gulin.tm.exception.system;

import ru.tsc.gulin.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
